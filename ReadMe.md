# Evaluation of Fast, Faster and Mask R-CNN regarding their inference times
![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)
![platform: linux](https://img.shields.io/badge/platform-linux-lightgrey)
![python: v3.10](https://img.shields.io/badge/python-v3.10-blue)
![detectron2: v0.6](https://img.shields.io/badge/detectron2-v0.6-blue)

This repo contains the necessary python code to run all three inferences on a custom coco dataset with [detectron2](https://github.com/facebookresearch/detectron2).

My term paper about this topic can be downloaded [here](https://michaelschmitz.gitlab.io/paper-fast_faster_mask-r-cnn-inference-comparision/Fast,%20Faster%20and%20Mask%20R-CNN:%20Working%20principles%20and%20comparison.pdf).

## Setup
A python3 installation is needed. No CUDA required, everything runs on CPU only.

Install all python dependencies with pip inside a virtual env (versions are tested on linux, other platforms might need to adjust).
```bash
# if virtualenv is not already installed
pip install virtualenv

# create venv and install requirements
python -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
# Seperate requiremetns.txt for detectron2 needed, 
# as torch is a build dependency, but pip only installs after building all dependencies
pip install -r requirements_2.txt
```

To use the coco dataset you can use the provided script in [datasets](datasets) (execute in containing folder).
It is based on the offical detectron2 script for test purposes, see [github](https://github.com/facebookresearch/detectron2/blob/main/datasets/prepare_for_tests.sh).

## Running
Simply execute the main.py. If you want to use the coco dataset (100 images), change the main call to this:
```python
if __name__ == "__main__":
    run_coco_dataset()
    # run_custom_dataset()
```
instead of this:
```python

if __name__ == "__main__":
    # run_coco_dataset()
    run_custom_dataset()
```

Note: For demo purposed most logging is turned off.

## Acknowledgements
Pictures in the custom dataset are taken from [tensorflow object_detection](https://github.com/tensorflow/models/tree/master/research/object_detection/test_images).
