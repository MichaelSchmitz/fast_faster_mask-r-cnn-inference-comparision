import logging
import pickle
import time
from os.path import exists
from detectron2.engine import DefaultPredictor

from detectron2.utils.logger import setup_logger
from detectron2.evaluation import inference_on_dataset

from rcnn_eval.selective_search import create_pickled_dict
from rcnn_eval.setup import load_model, create_dataloader, register_dataset, create_evaluator
from rcnn_eval.visualisation import create_visualised_images
import warnings

logger = setup_logger()

# just for demo purposes, to avoid cluttering of output
logger.level = logging.FATAL
warnings.filterwarnings("ignore")

models = [
    {
        'name': 'Fast R-CNN',
        'folder': 'fast',
        'url': 'COCO-Detection/fast_rcnn_R_50_FPN_1x.yaml'
    },
    {
        'name': 'Faster R-CNN',
        'folder': 'faster',
        'url': 'COCO-Detection/faster_rcnn_R_50_FPN_1x.yaml'
    },
    {
        'name': 'Mask R-CNN',
        'folder': 'mask',
        'url': 'COCO-InstanceSegmentation/mask_rcnn_R_50_FPN_1x.yaml'
    }
]


def run_custom_dataset():
    """
    Run inference on custom dataset, print execution times for every model
    """
    register_dataset('custom')
    if not exists('custom.pkl'):
        create_pickled_dict([
            (1, 'datasets/custom/val/image1.jpg'),
            (2, 'datasets/custom/val/image2.jpg'),
            (3, 'datasets/custom/val/image3.jpg')])
    times = open('times_custom.pkl', 'rb')
    selective_times = pickle.load(times)
    times.close()
    print('[INFO] Running inference for {} images'.format(len(selective_times)))
    print('[INFO] Selective search took a total of {:.4f}s'.format(sum(selective_times)))

    for model in models:
        config = load_model(model['url'], 'custom')
        predictor = DefaultPredictor(config)

        val_loader = create_dataloader(config, 'custom')
        evaluator = create_evaluator(config, 'custom', model['folder'])

        start = time.time()
        inference_on_dataset(predictor.model, val_loader, evaluator)
        end = time.time()

        print('[INFO] {} took {:.4f}s'.format(model['name'],
                                              end - start if model['name'] != 'Fast R-CNN' else
                                              end - start + sum(selective_times)))

        create_visualised_images('output/{}/coco_instances_results.json'.format(model['folder']),
                                 'custom', 'visualisation/{}'.format(model['folder']))


def run_coco_dataset():
    """
    Run inference on coco_2017_val dataset, print execution times for every model
    """
    print('[INFO] Running inference on coco dataset, using precomputed proposals for Fast R-CNN')
    for model in models:
        config = load_model(model['url'])
        predictor = DefaultPredictor(config)

        val_loader = create_dataloader(config, 'coco_2017_val')
        evaluator = create_evaluator(config, 'coco_2017_val', 'coco/{}'.format(model['folder']))

        start = time.time()
        inference_on_dataset(predictor.model, val_loader, evaluator)
        end = time.time()

        print('[INFO] {} took {:.4f}s'.format(model['name'], end - start))
        create_visualised_images('output/coco/{}/coco_instances_results.json'.format(model['folder']),
                                 'coco_2017_val', 'visualisation_coco/{}'.format(model['folder']))


if __name__ == "__main__":
    # run_coco_dataset()
    run_custom_dataset()
