from typing import List, Tuple
import time
import cv2
import numpy as np
import pickle


def selective_search(image_file: str, fast: bool = False) -> Tuple[np.ndarray, np.ndarray, float]:
    """
    Create region proposals for given image with fast or quality method
    Based on https://pyimagesearch.com/2020/06/29/opencv-selective-search-for-object-detection/

    :param image_file: File path to image
    :param fast: True -> Fast selective search, False -> quality selective search (optional, default: False)
    :return: Tuple of region proposals, objectness scores (all 0) and execution time
    """
    image = cv2.imread(image_file)
    ss = cv2.ximgproc.segmentation.createSelectiveSearchSegmentation()
    ss.setBaseImage(image)
    if fast:
        print("[INFO] using *fast* selective search")
        ss.switchToSelectiveSearchFast()
    else:
        print("[INFO] using *quality* selective search")
        ss.switchToSelectiveSearchQuality()

    # run selective search on the input image
    start = time.time()
    rects = ss.process()
    end = time.time()
    print("[INFO] selective search took {:.4f}s for {} and got {} region proposals"
          .format(end - start, image_file, len(rects)))

    return rects, np.zeros(len(rects), dtype=np.float32), end - start


def create_pickled_dict(images: List[Tuple[int, str]], fast: bool = False, out_file: str = 'custom.pkl') -> None:
    """
    Run selective search on all given images and create a pickled dict file for the region proposals
    as well as one for the execution times
    :param images: List of tuples with image_id and path
    :param fast: True -> Fast selective search, False -> quality selective search (optional, default: False)
    :param out_file: File to write pickled dict in (optional, default: 'custom.pkl')
    :return: List of execution times
    """
    ids = []
    boxes = []
    scores = []
    times = []
    for image in images:
        img_boxes, img_scores, img_times = selective_search(image[1], fast)
        ids.append(image[0])
        boxes.append(img_boxes)
        scores.append(img_scores)
        times.append(img_times)

    out = open(out_file, 'wb')
    pickle.dump(dict(ids=ids, boxes=boxes, objectness_logits=scores), out)
    out.close()

    out = open('times_{}'.format(out_file), 'wb')
    pickle.dump(times, out)
    out.close()
