from detectron2.data.datasets import register_coco_instances
from detectron2.evaluation import COCOEvaluator
from detectron2.config import get_cfg, CfgNode
from detectron2 import model_zoo
from detectron2.data import build_detection_test_loader
from torch.utils.data import DataLoader


def register_dataset(dataset_name: str) -> None:
    """
    Register the given dataset name as custom coco dataset
    Requirements:
      - instances_val.json in dataset/yourName/annotations
      - images in datasets/yourName/val
    :param dataset_name: Name of dataset to register
    """
    register_coco_instances(dataset_name, {}, 'datasets/{}/annotations/instances_val.json'.format(dataset_name),
                            'datasets/{}/val'.format(dataset_name))


def load_model(model: str, dataset: str = None) -> CfgNode:
    """
    Create a configuration node for given model from the model zoo
    Overrides the test dataset with given param (if set)
    For fast_rcnn the region proposal file is set as well
    :param model: relative path from detectron2/configs for model from model zoo
    :param dataset: name of custom dataset or None if using default
    :return: Configuration Node
    """
    cfg = get_cfg()
    cfg.merge_from_file(model_zoo.get_config_file(model))
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
    cfg.MODEL.WEIGHTS = model_zoo.get_checkpoint_url(model)
    if dataset is not None:
        cfg.DATASETS.TEST = (dataset, )
        if 'fast_rcnn' in model:
            cfg.DATASETS.PROPOSAL_FILES_TEST = ('{}.pkl'.format(dataset), )
    cfg.MODEL.DEVICE = "cpu"
    cfg.freeze()
    return cfg


def create_dataloader(config: CfgNode, dataset) -> DataLoader:
    """
    Create an dataloader for given dataset
    :param config: Configuration Node to use
    :param dataset: dataset to run on
    :return: Tuple of evaluator and DataLoader
    """
    return build_detection_test_loader(config, dataset)


def create_evaluator(config: CfgNode, dataset, output_subfolder: str) -> COCOEvaluator:
    """
    Create an evaluator for given dataset
    :param config: Configuration Node to use
    :param dataset: dataset to run on
    :param output_subfolder: name of subfolder where the results of inference should be stored
    :return: Tuple of evaluator and DataLoader
    """
    return COCOEvaluator(dataset, config, False, output_dir='output/{}'.format(output_subfolder))