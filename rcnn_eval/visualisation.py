# Based on https://github.com/facebookresearch/detectron2/blob/main/tools/visualize_json_results.py

import os
from typing import Tuple, List, Dict

import numpy as np
import json
import cv2
from collections import defaultdict
from detectron2.utils.file_io import PathManager
from detectron2.structures import Boxes, BoxMode, Instances
from detectron2.data import DatasetCatalog, MetadataCatalog, Metadata
from detectron2.utils.visualizer import Visualizer


def create_instances(predictions: List[Dict], image_size: Tuple[int, int], metadata: Metadata) -> Instances:
    """
    Create instances of the predicted classes for given image
    :param predictions:
    :param image_size:
    :param metadata:
    :return:
    """
    ret = Instances(image_size)

    score = np.asarray([x["score"] for x in predictions])
    chosen = (score > 0.5).nonzero()[0]
    score = score[chosen]
    bbox = np.asarray([predictions[i]["bbox"] for i in chosen]).reshape(-1, 4)
    bbox = BoxMode.convert(bbox, BoxMode.XYWH_ABS, BoxMode.XYXY_ABS)

    labels = np.asarray([metadata.thing_dataset_id_to_contiguous_id[predictions[i]["category_id"]] for i in chosen])

    ret.scores = score
    ret.pred_boxes = Boxes(bbox)
    ret.pred_classes = labels

    try:
        ret.pred_masks = [predictions[i]["segmentation"] for i in chosen]
    except KeyError:
        pass
    return ret


def create_visualised_images(input_file: str, dataset: str, output_folder: str) -> None:
    """
    Create images with inference results (bounding boxes, confidence and label, segmentation if available)

    :param input_file: JSON file containing inference results
    :param dataset: dataset the inference ran on
    :param output_folder: folder to store the new images
    """
    with PathManager.open(input_file, "r") as f:
        predictions = json.load(f)

    pred_by_image = defaultdict(list)
    for p in predictions:
        pred_by_image[p["image_id"]].append(p)

    dicts = list(DatasetCatalog.get(dataset))
    metadata = MetadataCatalog.get(dataset)

    os.makedirs(output_folder, exist_ok=True)

    for dic in dicts:
        img = cv2.imread(dic["file_name"], cv2.IMREAD_COLOR)[:, :, ::-1]
        basename = os.path.basename(dic["file_name"])

        predictions = create_instances(pred_by_image[dic["image_id"]], img.shape[:2], metadata)
        vis = Visualizer(img, metadata)
        vis_pred = vis.draw_instance_predictions(predictions).get_image()
        cv2.imwrite(os.path.join(output_folder, basename), vis_pred[:, :, ::-1])
